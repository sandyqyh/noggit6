# Findcasc.cmake is part of Noggit3, licensed via GNU General Public License (version 3).
# Bernd Lörwald <bloerwald+noggit@googlemail.com>

#find CASC
# CASC_LIBRARIES, the name of the library to link against
# CASC_FOUND, if false, do not try to link
# CASC_INCLUDES,

find_path(CASC_INCLUDE_DIR CASCLib.h CASCPort.h )
find_path(CASC_LIBRARY_DIR CASCLibRAD.lib )
find_library(CASC_LIBRARY casc)
set(CASC_LIBRARIES ${CASC_LIBRARY})
if( WIN32 )
  find_library(CASC_LIBRARYRAD CASCLibRAD.lib)
  find_library(CASC_LIBRARYDAD CASCLibDAD.lib)
  set(CASC_LIBRARIES ${CASC_LIBRARIES} ${CASC_LIBRARYRAD} ${CASC_LIBRARYDAD})
endif( WIN32 )

if(CASC_INCLUDE_DIR AND CASC_LIBRARY)
    SET(CASC_FOUND TRUE)
endif(CASC_INCLUDE_DIR AND CASC_LIBRARY)

set(CASC_INCLUDES ${CASC_INCLUDE_DIR})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(casc DEFAULT_MSG CASC_LIBRARY CASC_INCLUDE_DIR)

mark_as_advanced(CASC_INCLUDE_DIR CASC_LIBRARY CASC_LIBRARYRAD CASC_LIBRARYDAD)
